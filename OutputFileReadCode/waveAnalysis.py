'''
waveAnalysis.py by David Mathews: June 14, 2019.
Examples by Mark McCrea

For documentation, see the following google doc: 
https://docs.google.com/document/d/1fsO83cBJpac5VnUt56JP3ATrVh6Hxfwn_ALl2yVQwNg/edit?usp=sharing

This is a compilation of Python functions that are helpful when analyzing waveforms. 
In this code are two main sets of analysis methods: fitting with a difference of exponentials, and trapezoidal filter based energy and timestamp extraction.

To simply run one of either of these over a datafile, follow these examples.

One important note: Many of the functions have default values. If you want to override these default values, simply specify the one you want to override when calling the function.
	For example: doTrapAnalysis sets rise to 1000 by default. To call this function with a different rise, do the following.
		import waveAnalysis
		waveAnalysis.doTrapAnalysis('run21', rise=500)

Each function has a brief description before it along with what the inputs represent and should be.
Below are a few examples from Mark McCrea for operation on Windows Machines.

'''

#Example 1: Trapezoidal Filter Analysis
# (base) C:\location\gusername>python
# Python 3.7.0 (default, Jun 28 2018, 08:04:48) [MSC v.1912 64 bit (AMD64)] :: Anaconda, Inc. on win32
# Type "help", "copyright", "credits" or "license" for more information.
# >>> import os
# >>> os.chdir('d:\\Downloads')
# >>> import waveAnalysis 
# >>> waveAnalysis.doTrapAnalysis('d:\\run11')
# C:\gUsers\gusername\Anaconda3\lib\site-packages\mkl_fft\_numpy_fft.py:1044: FutureWarning: Using a non-tuple sequence for multidimensional indexing is deprecated; use `arr[tuple(seq)]` instead of `arr[seq]`. In the future this will be interpreted as an array index, `arr[np.array(seq)]`, which will result either in an error or a different result.
# output = mkl_fft.rfftn_numpy(a, s, axes)
# C:\Users\username\Anaconda3\lib\site-packages\numpy\core\fromnumeric.py:2920: RuntimeWarning: Mean of empty slice.
# out=out, **kwargs)
# C:\Users\username\Anaconda3\lib\site-packages\numpy\core\_methods.py:85: RuntimeWarning: invalid value encountered in double_scalars
# ret = ret.dtype.type(ret / rcount)
# >>>exit()



# Example 2: Plotting a given waveform from the file:
# (base) C:\Users\username>python
# Python 3.7.0 (default, Jun 28 2018, 08:04:48) [MSC v.1912 64 bit (AMD64)] :: Anaconda, Inc. on win32
# Type "help", "copyright", "credits" or "license" for more information.
# >>> import os
# >>> import os
# >>> os.chdir('d:\Downloads')
# >>> import trapFilter
# >>> headers,waveforms,wavelength = trapFilter.load('d:\\run11',800)
# >>> trapFilter.plotWave(waveforms[6],1000,100,1250,800)
# C:\Users\username\Anaconda3\lib\site-packages\mkl_fft\_numpy_fft.py:1044: FutureWarning: Using a non-tuple sequence for multidimensional indexing is deprecated; use `arr[tuple(seq)]` instead of `arr[seq]`. In the future this will be interpreted as an array index, `arr[np.array(seq)]`, which will result either in an error or a different result.
# output = mkl_fft.rfftn_numpy(a, s, axes)



import numpy as np
import os,sys
import struct
import matplotlib.pyplot as plt
import math
from scipy import signal
from scipy.stats import norm
from scipy.optimize import curve_fit
from scipy import interpolate
import time

'''
This function applies two transformations to every datapoint.
First the datapoint is converted from unsigned shorts to signed shorts
Second the entire waveform is shifted by the baseline offset
This is calculated by the mean of the first values (defined by the variable offset)
You should almost never need to use this (the other functions call it for you).

The first function does this transformation to one waveform, the second does it to multiple.
'''
def doAndShift(wave,offset):
	array=np.asarray(wave,dtype=np.int16)
	array[:]=array[:]&16383
	array[:]=array[:]-np.floor(array[:]/8192)*16384
	#now do the mean shift
	array[:]=array[:]-np.mean(array[0:int(offset)])
	array=array.astype(np.double)
	return array

def doAndShiftBulk(waves, offset):
	numwaves = len(waves)
	for i in range(numwaves):
		array = np.asarray(waves[i],dtype=np.int16)
		array[:]=array[:]&16383
		array[:]=array[:]-np.floor(array[:]/8192)*16384
		#now do the mean shift
		array[:]=array[:]-np.mean(array[0:int(offset)])
		waves[i]=array
	return waves

'''
This function loads the waveforms. It initially loads in one header to check the waveform length.
After that it assumes all waveforms have the same length and loads the rest.
It assumes a format of Bool (1byte), Int (4bytes), Int, Int, Long (8 bytes), Int and then waveform.
Note that this function loads the entirety of the file requested into RAM. This is fine for small files but once you files start to be a signifcant portion of your total RAM, don't use this.
Example: waveAnalysis.load('run11', 800)

The file path can be relative to the location where the script is running, but in general it is best to use absolute file paths. The single quotes are required
'''
def load(path,offset=800): #this function will load the waveforms and their headers
	size = os.stat(path).st_size
	waveformat='<'
	headers=[]
	waveforms=[]
	wavelength=0
	with open(path,"rb") as f:
		head=f.read(25)
		head = struct.unpack("<ciiiqi",head)
		wavelength = head[5]
	
	numwaves = size/(int(wavelength)*2+25) #assuming each waveform is 3500 shorts and a 25 byte header
	for i in range(wavelength):
		waveformat+='h'
	with open(path,"rb") as f:
		for i in range(int(numwaves)):
			head=f.read(25) #read header
			headers.append(struct.unpack("<ciiiqi",head)) #unpack header into container
			head=f.read(wavelength*2) #read waveform
			waveforms.append(struct.unpack(waveformat,head)) #unpack waveform into container
	waveforms = doAndShiftBulk(waveforms, offset)
	return headers,waveforms,wavelength

'''
This function defines a trapezoidal filter based on the input parameters.
It also scales the trapezoidal filter to properly calculate the energy.
Normally we do the division after the convolution to scale things but it can be done here with no issue as convolution is a linear operation.
Rise = how long in bins the trapezoid will be
Top = how long in bins the top will be (should be always greater than the rise time of the pulse)
Tau = decay constant (again in bins) of the waveform
num = how long the filter should be (always the length of the waveform) 
'''
def defineTrap(rise,top,tau,num): #this function defines the trapezoidal filter to be used
	filt=np.zeros(num)
	for i in range(rise):
		filt[i]=i+tau
		filt[i+rise+top]=rise-tau-i
	for i in range(rise,rise+top):
		filt[i]=rise
	for i in range(rise+rise+top,num):
		filt[i]=0
	scale=1.0/(rise*tau)
	filt*=scale
	return filt

'''
This function does the energy and t0 extraction from the convolved waveforms.
First it finds the maximum energy in the waveform and its location. 
Once that is known, it then determines when the output wave crosses a threshold (defined by the corresponding variable) relative to this max value in both the upwards and downwards direction (both sides of the trapezoid). 
Using this information, this function finds the middle of the trapezoid and extracts the energy from around that location. 
For t0 extraction, we know that the middle of the trapezoid should be half of the top length from the beginning of the trapezoid.
We also know how long the rise time is because that is defined. Using these two values the initial t0 of the trapezoid is found (this shift value is defined in t0shift).

waves=waveforms to be analyzed, it expects a list with 2 or more values
filt = numpy array or python list of the filter itself
t0shift = how far back to move from the midpoint of the trapezoid for the timing (calculated for you in the doTrapAnalysis function, also an integer)
threshold = percent of amplitude to cross over for energy extraction (must be less than 1)
'''
def energyt0(wave,filt,t0shift,threshold): 
	res=signal.convolve(wave,filt,mode='full')
	res=res[:len(filt)]
	#now find the max value and then the crossing points
	maxval=max(res)
	maxloc=np.argmax(res)
	crossval=threshold*maxval
	j=maxloc-t0shift
	leftcross=0
	while(j<maxloc and j < len(filt)-1):
		if(res[j]<=crossval and res[j+1]>=crossval):
			leftcross=j+0.5
			j=maxloc
		j=j+1
	rightcross=0
	j=maxloc
	while(j<maxloc+t0shift and j < len(filt)-1):
		if(res[j]>=crossval and res[j+1]<=crossval):
			rightcross=j+0.5
			j=maxloc+t0shift
		j=j+1
	midpoint=int((rightcross+leftcross)/2)
	energy=np.mean(res[midpoint-5:midpoint+5])
	if math.isnan(energy)==True:
		energy = -999999
	t0=midpoint-t0shift
	return energy,t0,maxloc,leftcross,rightcross


'''
These next two functions simply output the results to text files for easy analysis with other programs. 
'''
def outputTrapResults(outfile,num, header, energy, t0):
	#first figure out the output file name
	outfile.write(str(num)+' '+str(bool(header[0])) +' '+ str(int(header[1])) +' '+  str(int(header[2])) +' '+  str(int(header[3])) +' '+  str(header[4]) +' '+  str(int(header[5])) +' '+  str(float(energy)) +' '+  str(float(t0)) + '\n')
					

def outputFitResults(outfile,num, header, fitparameter):
	#first figure out the output file name
		outfile.write(str(num)+' '+str(bool(header[0])) +' '+ str(int(header[1])) +' '+  str(int(header[2])) +' '+  str(int(header[3])) +' '+  str(header[4]) +' '+  str(int(header[5])) +' '+  str(float(fitparameter[0])) +' '+  str(float(fitparameter[1])) + ' ' +str(float(fitparameter[2]))+ ' '+str(float(fitparameter[3]))+ ' '+str(float(fitparameter[4]))+'\n')


'''
This function defines the double exponential used for fitting. 
x = array
amp = amplitude of the pulse
decay1 = fall time of the waveform
decay2 = rise time of the waveform
t0 = when the waveform began 
'''
def doubleExp(x,amp, decay1, decay2, t0):
	y = np.zeros(len(x))
	for i in range(int(t0),len(x)):
		y[i] = amp*(np.exp(-(i-t0)/decay1)-np.exp(-(i-t0)/decay2))
	return y

'''
Here is where the fitting is actually done. This uses the scipy library function curve_fit to do the fitting.
For a performance increase, restrict the bounds more on the parameters being given to the fitting. 
For example if you were to fit all of the waveforms from one file and histogram the rise and fall time constants, it is probably safe to just use the mean of those in the future instead of allowing a large range of values (those should be fixed by electronics).
'''
def doFitting(waveform):
	xdata = np.arange(0,len(waveform))
	#the bounds on this fitting can be adjusted as you want
	#the first element represents the (lower then upper) bounds on the amplitude
	#the second element is for the decay constant
	#third is for the rise time constant
	#fourth is for t0
	popt,pcov = curve_fit(doubleExp,xdata,waveform,bounds=([0,800,0,700],[8192,1600,100,1500]))
	#popt,pcov = curve_fit(doubleExp,xdata,waveforms[i])
	
	#now calculate R^2 coefficient
	fitWave = doubleExp(xdata, popt[0],popt[1],popt[2],popt[3])
	ss_res=np.sum((waveform-fitWave)**2.0)
	ss_tot = np.sum((waveform-np.mean(waveform))**2.0)
	r2 = np.abs(1 - ss_res / ss_tot)
	output=popt.tolist()
	output.append(r2)
	return output


'''
This function handles the input and makes it so it is always a list. 
The goal is to make the other functions more compact.
'''
def boardChannelConversion(bc):
	if isinstance(bc,list):
		return bc
	else:
		return [bc,]



'''
This function calls several of the other functions in order to do the full analysis process on one waveform file with the trapezoidal filter.
filename = name and location of file to analyze
offset = how many data points to use for averaging out the baseline shift
rise = trapezoid rise time constant
top = trapezoid top constant
tau = trapezoid decay constant
threshold = energy extraction threshold

Board and channel when specified will cause this function to only analyze waveforms from those boards and channels. 
Otherwise this code will run over all waveforms in the file.
'''

def doTrapAnalysis(filename, offset=800, rise=1000, top=100, tau=1250, threshold=0.8, bc=(-1,-1), output=False):
	#get file information
	#open the file to be analyzed
	size = os.stat(filename).st_size
	waveformat='<'
	wavelength=0
	with open(filename,"rb") as f:
		head=f.read(25)
		head = struct.unpack("<ciiiqi",head)
		wavelength = head[5]
	#determine how many waveforms there are	
	numwaves = size/(int(wavelength)*2+25) #assuming each waveform is 3500 shorts and a 25 byte header
	for i in range(wavelength):
		waveformat+='h'
	
	#go ahead and determine the filter
	filt = defineTrap(rise, top, tau, wavelength) #i assume a waveform length of 3500 here
	t0shift=int(0.5*top + rise)
		
	#now handle the output file name and open it
	outstart, extension = os.path.splitext(filename)
	outname=outstart
	#now handle the boards and channels
	bc=boardChannelConversion(bc)	
	if bc[0]==(-1,-1):
		outname=outstart+'_trapres.txt'
	else:
		for a,b in bc:
			outname+='bc'+str(a)+str(b)
		outname+='_trapres.txt'
	fout = open(outname,"w+")
	fout.write('EntryNumber Result EventID Board Channel Timestamp Length Energy T0\n')
	#now both files are open and ready to be written to
	#set up the buffers now for the histogram
	energies=[]
	for i in range(len(bc)):
		energies.append([])
	num=0
	with open(filename,"rb") as f:
		for i in range(int(numwaves)):
			head=f.read(25) #read header
			head=struct.unpack("<ciiiqi",head) #unpack header into container
			wave=f.read(wavelength*2) #read waveform
			wave=struct.unpack(waveformat,wave)
			waveBC=(int(head[2]),int(head[3]))
			#now check to see if we want to do the analysis on this waveform or not
			if (waveBC in bc) or (bc[0]==(-1,-1)):
				#convert the waveform to signed 16 bit
				wave=doAndShift(wave,offset)
				energy,t0,maxloc,lcross,rcross = energyt0(wave,filt,t0shift,threshold)		
				#now append this energy to the proper list
				if bc[0]==(-1,-1):
					energies[0].append(energy)
				else:
					energies[bc.index(waveBC)].append(energy)
				#now write these results to the output file
				outputTrapResults(fout,num,head,energy,t0)	
				num+=1
	#now close the output file
	fout.close()
	
	#now check to see if any of the pixels didn't go off
	#if they did just put a large negative value in there so the histogram doesn't care
	for i in range(len(energies)):
		if not energies[i]:
			energies[i].append(-10000)
				
	#now make the histogram
	#edit these parameters as you wish
	binwidth = 2
	startenergy = 0
	maxEner=-100000
	for i in range(len(bc)):
		trialMax=max(energies[i])
		if trialMax>=maxEner:
			maxEner=trialMax
	stopenergy = np.ceil(maxEner/binwidth)*binwidth
	bins=np.arange(startenergy,stopenergy,binwidth)
	if output==False:
		for i in range(len(bc)):
			plt.hist(energies[i],bins=bins,label='bc'+str(bc[i][0])+str(bc[i][1]),histtype='step')	
		#for logarithmic scaling on the y axis, uncomment this next line
		#plt.yscale('log')
		outstart, extension = os.path.splitext(filename)
		pdfname=outstart
		title=outstart
		if bc[0]==(-1,-1):
			title = outstart+' (all Boards and Channels) Energy Histogram'
			pdfname=outstart+'_traphist.pdf'	
		else:
			title = outstart+' Energy Histogram'
			pdfname=outstart
			for a,b in bc:
				pdfname+='bc'+str(a)+str(b)
			pdfname+='traphist.pdf'	
		plt.xlabel('Energy (ADC)')
		plt.ylabel('Count')
		plt.legend(loc='upper right')
		plt.title(title)
		#determine output file name	
		plt.savefig(pdfname)
		plt.show()
		plt.clf()
	else:
		return bins,energies

'''
Here is the overal analysis function for waveform fitting. 
This does the fitting and then prints the fit parameters to a file.
filename = path to the file to analyze
offset = how many bins to average to shift baseline to 0
'''
def doFitAnalysis(filename, offset=800, bc=(-1,-1), output=False):
	size = os.stat(filename).st_size
	waveformat='<'
	wavelength=0
	with open(filename,"rb") as f:
		head=f.read(25)
		head = struct.unpack("<ciiiqi",head)
		wavelength = head[5]
	#determine how many waveforms there are	
	numwaves = size/(int(wavelength)*2+25) #assuming each waveform is 3500 shorts and a 25 byte header
	for i in range(wavelength):
		waveformat+='h'
			
	bc=boardChannelConversion(bc)	
	outstart, extension = os.path.splitext(filename)
	outname=outstart
	if bc[0]==(-1,-1):
		outname=outstart+'_fitres.txt'
	else:
		for a,b in bc:
			outname+='bc'+str(a)+str(b)
		outname+='_fitres.txt'
	fout = open(outname,"w+")
	fout.write('EntryNumber Result EventID Board Channel Timestamp Length Amplitude DecayConst RiseConst t0 FitStrength(R^2)\n')
	#now both files are open and ready to be written to
	#do the fitting now
	num=0
	res=[]
	with open(filename,"rb") as f:
		for i in range(int(numwaves)):
			head=f.read(25) #read header
			head=struct.unpack("<ciiiqi",head) #unpack header into container
			wave=f.read(wavelength*2) #read waveform
			wave=struct.unpack(waveformat,wave)
			#now check to see if we want to do the analysis on this waveform or not
			waveBC=(int(head[2]),int(head[3]))
			if (waveBC in bc) or bc[0]==(-1,-1):
				wave=doAndShift(wave,offset)
				fitResults=doFitting(wave)
				res.append(fitResults)
				print(str(num)+' ' + str(fitResults[0]) + ' ' + str(fitResults[1])+ ' '+str(fitResults[2]) + ' ' + str(fitResults[3])+ ' '+str(fitResults[4]))
				outputFitResults(fout,num,head,fitResults)
				num+=1	
	fout.close()
	if output==True:
		return res
	else:
		return
'''
This function will plot a specific waveform and the result of convolving that waveform with a trapezoidal filter of choice.
'''
def plotWaveTrap(waveform, rise=1000, top=100, tau=1250):
	filt = defineTrap(rise, top, tau, len(waveform)) #i assume a waveform length of 3500 here
	plt.plot(waveform)
	res=signal.convolve(waveform,filt,mode='full')
	res=res[:len(filt)]
	plt.plot(res)
	plt.show()
	plt.clf()
'''
Here is the analogous function to plotWaveTrap with fitting instead.
'''
def plotWaveFit(waveform, amp, decay, risetime, t0):
	plt.plot(waveform)
	x = np.arange(len(waveform))
	fitWave=doubleExp(x,amp,decay,risetime,t0)	
	plt.plot(fitWave)
	plt.show()
	plt.clf()
'''
This function will show the user images of each waveform in a particular energy range. 
The purpose of this is to "zoom" in on particular energy peaks and analyze the type of waveforms appearing there.
It can output all of the waveforms to files if desired as well depending on user input.
By setting printFiles = True, each image viewed will also be saved to a file.
By setting printDontView = True, nothing will be shown to the user, but all waveforms will be saved to a file. 
It is possible to save individual waveforms through the window that appears when viewing them as well.

The final two inputs are board and channel. If they aren't specified, it will show you every waveform. Otherwise it will only show you waveforms from the specific board and channel.

To stop this function you need to just cancel the whole script. Otherwise it will keep going until you run out of waveforms to view.
'''
def plotRange(filename, lowEner, highEner, offset=800, rise=1000, top=100, tau=1250, threshold=0.8, printFiles = False, printDontView = False, bc=(-1,-1)):
	#get file information
	#open the file to be analyzed
	size = os.stat(filename).st_size
	waveformat='<'
	wavelength=0
	with open(filename,"rb") as f:
		head=f.read(25)
		head = struct.unpack("<ciiiqi",head)
		wavelength = head[5]
	#determine how many waveforms there are	
	numwaves = size/(int(wavelength)*2+25) #assuming each waveform is 3500 shorts and a 25 byte header
	for i in range(wavelength):
		waveformat+='h'
	
	#go ahead and determine the filter
	filt = defineTrap(rise, top, tau, wavelength) #i assume a waveform length of 3500 here
	t0shift=int(0.5*top + rise)
	bc = boardChannelConversion(bc)	
	num = 0
	with open(filename,"rb") as f:
		for i in range(int(numwaves)):
			head=f.read(25) #read header
			head=struct.unpack("<ciiiqi",head) #unpack header into container
			wave=f.read(wavelength*2) #read waveform
			wave=struct.unpack(waveformat,wave)
			#now check to see if we want to do the analysis on this waveform or not
			waveBC=(int(head[2]),int(head[3]))
			if (waveBC in bc) or bc[0]==(-1,-1):
				#convert the waveform to signed 16 bit
				wave=doAndShift(wave,offset)
				energy,t0,maxloc,lcross,rcross = energyt0(wave,filt,t0shift,threshold)		
				if energy>= lowEner and energy<=highEner:
					plt.plot(wave)
					plt.title('Waveform '+str(num) +' Board='+str(waveBC[0])+' Channel='+str(waveBC[1])+' with Energy='+str(energy)+' and t0='+str(t0))
					res=signal.convolve(wave,filt,mode='full')
					res=res[:len(filt)]
					plt.plot(res)
					outstart, extension = os.path.splitext(filename)
					outfile = outstart + '_'+str(waveBC[0])+str(waveBC[1])+'wave'+str(num)+'.png'
					if bc[0][0] == -1 and bc[0][1] == -1:
						outfile = outstart + '_wave'+str(num)+'.png'
					if printDontView == True:
						outfile = outstart + '_wave'+str(num)+'.png'
						plt.savefig(outfile)
						plt.clf()	
					elif printFiles == True:
						outstart, extension = os.path.splitext(filename)
						outfile = outstart + '_wave'+str(num)+'.png'
						plt.savefig(outfile)
						plt.show()
						plt.clf()
					else:
						plt.show()
						plt.clf()
				num+=1
	return

'''
This function returns a noise spectrum for the provided waveforms.
filename = name of file to do analysis on
offset = how much of each waveform to include (note that this code always starts at index 0 and moves on to offset from there)
	If you want to include the whole waveform, use offset=-1
board and channel can be specified. 
Every time this is run it will output a .png file with the spectrum. 
The name of this file depents on the data file given and which boards and channels were specified.
'''

def getPowerSpectrum(filename, offset=800,bc=(-1,-1), output=False):
	#first load in the waveforms
	size = os.stat(filename).st_size
	waveformat='<'
	wavelength=0
	with open(filename,"rb") as f:
		head=f.read(25)
		head = struct.unpack("<ciiiqi",head)
		wavelength = head[5]
	#determine how many waveforms there are	
	numwaves = size/(int(wavelength)*2+25) #assuming each waveform is 3500 shorts and a 25 byte header
	for i in range(wavelength):
		waveformat+='h'
	readlength = wavelength
	if offset == -1:
		offset = wavelength

	#convert the single board channel to a list to match the behavior for multiple boards and channels
	bc=boardChannelConversion(bc)	
	#now create the containers for the results from multple boards and channels
	averageRes=[]
	RMS = []
	num = []
	for i in range(len(bc)):
		averageRes.append(np.zeros(int(offset/2)+1))	
		RMS.append([])
		num.append(0)
	x = []
		
	with open(filename,"rb") as f:
		for i in range(int(numwaves)):
			head=f.read(25) #read header
			head=struct.unpack("<ciiiqi",head) #unpack header into container
			wave=f.read(readlength*2) #read waveform
			wave=struct.unpack(waveformat,wave)
			#now check to see if we want to do the analysis on this waveform or not
			waveBC=(int(head[2]),int(head[3]))	
			if (waveBC in bc) or bc[0]==(-1,-1):
				wave=doAndShift(wave,offset)
				#add it to the buffer for that pixel
				#fs value is set to 250000000 as our sampling rate is every 4 ns
				x,y = signal.periodogram(wave[:offset],scaling='spectrum',fs=250000000)
				y[0]=0
				if bc[0]==(-1,-1):
					averageRes[0]=np.add(averageRes[0],y)
					num[0]+=1
					RMS[0].append(np.sqrt(np.mean(np.square(wave[:offset]))))
				else:
					averageRes[bc.index(waveBC)]=np.add(averageRes[bc.index(waveBC)],y)
					num[bc.index(waveBC)]+=1
					RMS[bc.index(waveBC)].append(np.sqrt(np.mean(np.square(wave[:offset]))))
	averageRMS = []
	RMSSTD = []
	for i in range(len(bc)):
		averageRMS.append(np.mean(RMS[i]))
		RMSSTD.append(np.std(RMS[i]))
		averageRes[i]=np.divide(averageRes[i],num[i])
	#output the results in MHz
	x=x/1000000.0
	#calculate the spectral density
	#now output the results			
	if output==False:
		outstart, extension = os.path.splitext(filename)
		for i in range(len(averageRes)):
			plt.plot(x,averageRes[i],label='bc'+str(bc[i][0])+str(bc[i][1]))
		plt.xlabel('Frequency (MHz)')
		plt.ylabel(' (ADC Energy)^2/Hz')
		plt.yscale('log')
		title = 'Power Spectral Density for '+str(outstart)
		outtitle=outstart+'_freqspec'
		if bc[0]==(-1,-1):
			outtitle+='all.png'
		else:
			for i in range(len(bc)):		
				outtitle+='bc'+str(bc[i][0])+str(bc[i][1])
			outtitle+='.png'
		plt.title(title)
		plt.legend(loc='upper right')
		plt.savefig(outtitle)
		plt.show()
		plt.clf()
		return
	else:
		return x, averageRes, averageRMS, RMSSTD


'''
This function simulates the electronics response to a pulse. This was written by Aaron Jezghani
and has been adapted for Python
Currently there is no randomization in the waveform production, that will be added.
'''
def genPulse(amp, length):
	ccSlow = 2.5 #slow charge collection time
	ccSlow = ccSlow/(ccSlow+1)
	ccFast = 1.0/2.5  #fast charge collection time
	alphaCr = 1250/(1250+1) #fall time of output
	alphaRc1 = 1/2.75
	alphaRc2 = 1/2.75
	step=[0,0]
	charge = [0,0]
	curS=[0,0]
	curF=[0,0]
	cr=[0,0]
	rc1 = [0,0]
	rc2 = [0,0]
	t0 = 1000
	output = np.zeros(length)
	for i in range(length):
		step[i%2] = 1 if i>=t0 else 0
		curS[i%2]=ccSlow*(curS[(i+1)%2]+step[i%2]-step[(i+1)%2])
		curF[i%2]=ccFast*(curS[i%2]-curF[(i+1)%2])+curF[(i+1)%2]
		charge[i%2]=charge[(i+1)%2]+amp*curF[i%2]*(1./ccSlow-1.)
		cr[i%2]=alphaCr*(cr[(i+1)%2]+charge[i%2]-charge[(i+1)%2])
		rc1[i%2]=alphaRc1*(cr[i%2]-rc1[(i+1)%2])+rc1[(i+1)%2]
		rc2[i%2]=alphaRc2*(rc1[i%2]-rc2[(i+1)%2])+rc2[(i+1)%2]
		output[i]=rc2[i%2]
	return output


'''
This function simply finds how long each wave in the file is
'''
def getWavelength(filename):
	wavelength = 0
	with open(filename,"rb") as f:
		head=f.read(25)
		head = struct.unpack("<ciiiqi",head)
		wavelength = head[5]
	return wavelength



'''
This function determines what boards and channels were present in a particular file and returns two lists. These lists are set up so each combo of boards and channels is represented once.
'''
def getBoardsChannels(filename):
	size = os.stat(filename).st_size
	waveformat='<'
	wavelength=0
	with open(filename,"rb") as f:
		head=f.read(25)
		head = struct.unpack("<ciiiqi",head)
		wavelength = head[5]
	numwaves = size/(int(wavelength)*2+25) #assuming each waveform is 3500 shorts and a 25 byte header
	for i in range(wavelength):
		waveformat+='h'
	boardChannel=[]	
	with open(filename,"rb") as f:
		for i in range(int(numwaves)):
			head=f.read(25) #read header
			header=struct.unpack("<ciiiqi",head) #unpack header into container
			head=f.read(wavelength*2) #read waveform but throw it out this time, don't need to store it
			#now see if the board and channel combo is in the list already
			if (int(header[2]),int(header[3])) not in boardChannel:
				boardChannel.append((int(header[2]),int(header[3])))
	#now get the unique values for the boards and channels
	return boardChannel


'''
This function generates a fake waveform based on the given Power spectrum
wavelength is the desired length of the waveform
fftlength is the length of the data the power spectrum was generated from
powerSpectrum is of course the power spectrum
realRMS is the rms of the noise to be used
energy is the energy in the waveform
t0 is the time at which the waveform begins
'''
def simulateNoisyWave(wavelength,fftlength,powerSpectrum,realRMS,energy,t0=1000):
	#first simulate an ideal wave
	wave=genPulse(energy,wavelength)
	#now generate the fake noise
	#first make an empty array of complex values
	noiseFFT = np.zeros(len(np.fft.fftfreq(fftlength)),dtype=np.complex64)
	#now assign a random phase to each frequency in the spectrum
	amplitudes=np.sqrt(powerSpectrum[:]/2.0)	
	for i in range(len(powerSpectrum)):
		phase = 2*np.pi*np.random.random_sample()
		noiseFFT[i] = complex(amplitudes[i]*np.cos(phase),amplitudes[i]*np.sin(phase))
		noiseFFT[-i]= complex(amplitudes[i]*np.cos(phase),-1*amplitudes[i]*np.sin(phase))
	noise = np.fft.fft(noiseFFT)
	while(len(noise)<wavelength):
		noise = np.concatenate((noise,noise))
	noise=noise[:wavelength]
	#now make sure the RMS is set to the average
	noiseRMS = np.sqrt(np.mean(np.square(noise[:].real)))
	ratio=realRMS/noiseRMS
	noise = np.multiply(noise[:].real,ratio)
	wave=np.add(wave,noise[:].real)
	return wave


'''
The goal of this function is to output the fit parameters for particular inputs.
This way if you run with the same inputs as a previous run, you don't have to wait as long
Note that the inupts do have to be EXACTLY the same, I don't allow flexibility in this currently.
'''
def checkCorrectionTable(filename,noiseFile,offset,rise,top,tau,threshold,board,channel,numSim,numStep):
	#now search the file for the particular entry we are looking for
	fitM=False
	fitB=False
	exists = os.path.isfile('fitTable.txt')
	if exists:
		#figure out how long the file is
		with open('fitTable.txt',"r") as f:
			lines = f.readlines()
			#get past the two header lines
			#now read the rest of the file
			for i in range(len(lines)):
				line = lines[i].split()
				#check to see if that line is a comment or not
				if line[0]!="#":
					if (str(line[0])==str(filename) and str(line[1])==str(noiseFile) and str(line[2])==str(offset) and str(line[3])==str(rise) and str(line[4])==str(top) and str(line[5])==str(tau) and str(line[6])==str(threshold) and str(line[7])==str(board) and str(line[8])==str(channel) and str(line[9])==str(numSim) and str(line[10])==str(numStep) and fitM==False and fitB==False):
						fitM = float(line[11])
						fitB = float(line[12])
	else:
		f = open('fitTable.txt',"w")
		f.write('# Generated by waveAnalysis.py by the checkCorrectionTable function\n')
		f.write("# To comment, add a # followed by a space before your comment as done in these lines\n")
		f.write('# Filename noiseFile Offset Rise Top Tau Threshold Board Channel numSim numStep fitM(slope) fitB(yinter)\n')
		f.close()
	return fitM,fitB
 
def writeCorrectionTable(filename,noiseFile,offset,rise,top,tau,threshold,board,channel,numSim,numStep,fitM,fitB):
	#this function appends to the end of the table the most recent fit parameters
	f = open('fitTable.txt',"a")
	f.write(str(filename)+ ' ' + str(noiseFile) + ' ' + str(offset) + ' '+str(rise) + ' ' + str(top) + ' ' + str(tau) + ' ' + str(threshold) + ' ' + str(board) + ' ' + str(channel) + ' ' + str(numSim) + ' ' + str(numStep) +' ' + str(fitM) + ' ' + str(fitB) + '\n')			
	f.close()

	
'''
This function tries to find the correction to the trapezoidal filter energy output. 
It takes an input file to determine the noise spectrum.
Then it creates waveforms with a range of energies with that noise spectrum superimposed
Once this is done the difference in real energy and energy output from the trapezoidal filter
is found so it can be corrected for.

This function cannot be used to correct multiple pixels at a time yet. It can only handle one or it treats them all as one pixel. 
'''
def energyCorrection(filename, forNoise=False, offset=800, rise=1000, top=100, tau=1250, threshold=0.8, bc=(-1,-1), numSim=1000, numStep=12):
	#determine the energies that the code would normally output
	bins,energies = doTrapAnalysis(filename, offset=offset, bc=bc, rise=rise,top=top,tau=tau,threshold=threshold,output=True)
	noiseFile = filename
	noiseOffset=offset
	if(forNoise!=False):
		noiseFile=forNoise
		noiseOffset=-1
	#check to see if the correction has already been determined for these input values
	#if so just use those values
	fitM,fitB = checkCorrectionTable(filename.split('/')[-1],forNoise.split('/')[-1],offset,rise,top,tau,threshold,bc[0],bc[1],numSim,numStep)
	if fitM == False:
		np.random.seed(int(time.time()))	
		#determine range of energies to simulate waveforms in
		enerRange = np.logspace(5,12,num=numStep,base=2)	
		#now simulate waveforms at each energy step with the proper noise spectrum superimposed
		wavelength = getWavelength(filename)			
		meanEners=[]
		#first determine the noise spectrum
		freqs, powerSpectrum, noiseRMS, noiseRMSSTD = getPowerSpectrum(noiseFile,offset=noiseOffset,bc=bc,output=True)
		fftlen=noiseOffset
		if fftlen==-1:
			fftlen=getWavelength(noiseFile)
		#make filter
		filt=defineTrap(rise=rise,top=top,tau=tau,num=wavelength)	
		t0shift=int(0.5*top+rise)
		for i in enerRange: #iterate over the energies desired
			print('Starting Energy '+str(i) + ' : '+str(enerRange[-1]))
			energy = i
			meanEner = 0
			for j in range(numSim):     #iterate over the number of waveforms simulated at each energy
				#create the fake waveform	
				#determine what RMS to use
				rms = np.random.normal(noiseRMS,noiseRMSSTD)
				simulWave = simulateNoisyWave(wavelength,fftlen,powerSpectrum,rms,energy)
				#now analyze this fake waveform
				simulWave = doAndShift(simulWave,offset)
				energyRes,t0,maxloc,left,right	= energyt0(simulWave,filt,t0shift,threshold) 
				meanEner+=energy-energyRes
			meanEner=meanEner/numSim
			meanEners.append(meanEner)
		#now that the energy correction is known across the range, fit this to a function
		fitVals=np.polyfit(enerRange,meanEners,1)
		fitM = fitVals[0]
		fitB = fitVals[1]
		#now write these new parameters to the file
		writeCorrectionTable(filename,noiseFile,offset,rise,top,tau,threshold,bc[0],bc[1],numSim,numStep,fitM,fitB)
	#now do the correction
	trueEnergies=np.zeros(len(energies))
	for i in range(len(energies)):
		trueEnergies[i]=energies[i]+energies[i]*fitM+fitB
	plt.hist(energies,bins=bins,histtype='step',label='original')
	plt.hist(trueEnergies,bins=bins,histtype='step',label='calibrated')
	plt.legend(loc='upper right')
	plt.show()


def analyzePedestal(filename, offset=800, rise=1000, top=100, tau=1250, threshold=0.8, bc=(-1,-1), numSim=1000, trigger=10, wavelength=3500, enerMean=0, enerSTD=10, output=False):
	#first get the noise spectrum of the file being analyzed
	freqs, powerSpectrum, noiseRMS, noiseRMSSTD = getPowerSpectrum(filename,offset=offset,bc=bc,output=True)
	#define the filter to be used
	filt=defineTrap(rise=rise,top=top,tau=tau,num=wavelength)	
	t0shift=int(0.5*top+rise)
	fftlen=offset
	if fftlen == -1:
		fftlen=getWavelength(filename)
	energies=[]#original energies
	results=[]#after trigger logic
	for i in range(numSim):
		energy=np.random.normal(enerMean,enerSTD)
		energy=np.abs(energy)
		energies.append(energy)
		simulWave = simulateNoisyWave(wavelength,fftlen,powerSpectrum,noiseRMS,energy)
		simulWave = doAndShift(simulWave,offset)
		#now do the analysis on the waveform
		res=signal.convolve(simulWave,filt,mode='full')
		res=res[:len(filt)]
		doesTrigger = np.any(res[:]>trigger)
		if doesTrigger:
			energyRes,t0,maxloc,left,right = energyt0(simulWave,filt,t0shift,threshold)
			results.append(energyRes)
	if output==False:
		lowBin=trigger-enerSTD
		highBin=enerMean+enerSTD*5 #there shouldn't be anything out that far realistically
		bins=np.arange(lowBin,highBin,(highBin-lowBin)/100)#makes 100 bins that span the whole range
		plt.hist(energies,bins=bins,label='original peak',histtype='step')
		plt.hist(results,bins=bins,label='after triggering',histtype='step')
		plt.legend(loc='upper right')
		plt.show()
	else:
		return energies,results


'''
This function is just an easy way to compare the power spectrum from multiple files
'''
def comparePowerSpectrum(files,offset=800, bc=(-1,-1)):
	bc=boardChannelConversion(bc)
	filename='powerSpectrum'
	for f in files:
		filename+=f.split('/')[-1]
		results=getPowerSpectrum(f,offset=offset,bc=bc,output=True)
		for i in range(len(bc)):
			legend=f.split('/')[-1]+'bc'+str(bc[i][0])+str(bc[i][1])
			plt.plot(results[0],results[1][i],label=legend)
	for i in range(len(bc)):
		filename+='bc'+str(bc[i][0])+str(bc[i][1])
	plt.legend(loc='upper right')
	plt.title('Comparing Power Spectrums')
	plt.xlabel('Frequency (MHz)')
	plt.ylabel('Energg^2/Hz (ADC^2/Hz)')
	plt.yscale('log')
	filename+='.png'
	plt.savefig(filename)
	plt.show()

'''
This funciton is the energy spectrum analog to the power spectrum comparison function above.
'''
def compareEnergySpectrum(files, offset=800, rise=1000, top=100, tau=1250, threshold=0.8, bc=(-1,-1)):
	bc=boardChannelConversion(bc)
	filename= 'energySpectrum'
	for f in files:
		filename+=f.split('/')[-1]
		results=doTrapAnalysis(f, offset=offset, rise=rise, top=top, tau=tau, threshold=threshold, bc=bc, output=True)
		for i in range(len(bc)):
			legend=f.split('/')[-1]+'bc'+str(bc[i][0])+str(bc[i][1])
			plt.hist(results[1][i],bins=results[0],density=True,label=legend,histtype='step')
	plt.xlabel('Energy')
	plt.ylabel('Normalized Count')
	plt.legend(loc='upper right')
	filename+='.png'
	plt.savefig(filename)
	plt.show()


