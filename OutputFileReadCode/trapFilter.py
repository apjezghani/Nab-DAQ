
# This python script has two functions for processing binary output files from Nab-DAQ.vi.

# The first function is doAnalysis(path, offset, rise, top, tau, threshold) which produces a text file containing the header information and energy of each event as determined using a trapezoid filter.

# The first parameter is the path to and file name for processing.  The file name can be a relative path compared to the location of trapFilter.py or an absolute path. It uses windows style file paths with double backslashes in place of the usual single backslashes for folders. For exmaple 'c:\\foldername\\filename'.

# The second paramter, offset, is the number of time bins at the start of the event that will be averaged to calculate the baseline for subtraction before applying the trapezoid filter.  It is an integer.

# The third paramters, rise, is the trapezoid filter rise time.  It is an integer.  A good default is 1000.

# The fourth paramter, top, is the width of the flat top for the trapezoid filter.  It is an integer.  A good default is 100.

# The fifth paramter, tau, is the decay constant for the input pulse. It is an integer.  A good default is 1250.  If the decay time is too long the flat top of the trapezoid will tilt downards to the right.  If the decay time is too short the flat top will tilt upwards.

# The sixth paramter, threshold, is used to find the event start time of the event by taking the location on the rising and falling slopes of the trapezoid at threshold times the flat top maximum value and linearly extrapolating towards zero. It is a decimal number that varies from higher than 0 to less than 1. A good default is 0.8.


# The second functions of interest are:
# load(path,offset)
# plotWave(waveform, rise, top, tau, threshold):

# after load() is run then waveforms can be plotted on screen along with the resulting trapezoid from the filter.  See the second example below for the correct syntax.  The filter paramters as the sames as for doAnalysis() above.

# Waveforms can be saved out using the disk icon in the upper right.
# After a waveform is plotted it's window must be closed before plotting the next waveform. 


# (base) C:\location\gusername>python
# Python 3.7.0 (default, Jun 28 2018, 08:04:48) [MSC v.1912 64 bit (AMD64)] :: Anaconda, Inc. on win32
# Type "help", "copyright", "credits" or "license" for more information.
# >>> import os
# >>> os.chdir('d:\\Downloads')
# >>> import trapFilter 
# >>> trapFilter.doAnalysis('d:\\run11',800,1000,100,1250,0.8)
# C:\gUsers\gusername\Anaconda3\lib\site-packages\mkl_fft\_numpy_fft.py:1044: FutureWarning: Using a non-tuple sequence for multidimensional indexing is deprecated; use `arr[tuple(seq)]` instead of `arr[seq]`. In the future this will be interpreted as an array index, `arr[np.array(seq)]`, which will result either in an error or a different result.
# output = mkl_fft.rfftn_numpy(a, s, axes)
# C:\Users\username\Anaconda3\lib\site-packages\numpy\core\fromnumeric.py:2920: RuntimeWarning: Mean of empty slice.
# out=out, **kwargs)
# C:\Users\username\Anaconda3\lib\site-packages\numpy\core\_methods.py:85: RuntimeWarning: invalid value encountered in double_scalars
# ret = ret.dtype.type(ret / rcount)
# >>>exit()



# Example of plotting a given waveform from the file:
# (base) C:\Users\username>python
# Python 3.7.0 (default, Jun 28 2018, 08:04:48) [MSC v.1912 64 bit (AMD64)] :: Anaconda, Inc. on win32
# Type "help", "copyright", "credits" or "license" for more information.
# >>> import os
# >>> import os
# >>> os.chdir('d:\Downloads')
# >>> import trapFilter
# >>> headers,waveforms,wavelength = trapFilter.load('d:\\run11',800)
# >>> trapFilter.plotWave(waveforms[6],1000,100,1250,800)
# C:\Users\username\Anaconda3\lib\site-packages\mkl_fft\_numpy_fft.py:1044: FutureWarning: Using a non-tuple sequence for multidimensional indexing is deprecated; use `arr[tuple(seq)]` instead of `arr[seq]`. In the future this will be interpreted as an array index, `arr[np.array(seq)]`, which will result either in an error or a different result.
# output = mkl_fft.rfftn_numpy(a, s, axes)



# created by: David Mathews 2019/05/08
# instructions and examples by Mark McCrea 2019/05/08.


import numpy as np
import os,sys
import struct
import matplotlib.pyplot as plt
import math
from scipy import signal
from scipy.stats import norm

def doandshift(waves, offset):
	numwaves = len(waves)
	for i in range(numwaves):
		array = np.asarray(waves[i],dtype=np.int16)
		array[:]=array[:]&16383
		array[:]=array[:]-np.floor(array[:]/8192)*16384
		#now do the mean shift
		array[:]=array[:]-np.mean(array[0:int(offset)])
		waves[i]=array
	return waves


def load(path,offset): #this function will load the waveforms and their headers
	size = os.stat(path).st_size
	waveformat=''
	headers=[]
	waveforms=[]
	wavelength=0
	with open(path,"rb") as f:
		head=f.read(25)
		head = struct.unpack("<ciiiqi",head)
		wavelength = head[5]
		
	numwaves = size/(int(wavelength)*2+25) #assuming each waveform is 3500 shorts and a 25 byte header
	for i in range(wavelength):
		waveformat+='h'
	with open(path,"rb") as f:
		for i in range(int(numwaves)):
			head=f.read(25) #read header
			headers.append(struct.unpack("<ciiiqi",head)) #unpack header into container
			head=f.read(wavelength*2) #read waveform
			waveforms.append(struct.unpack(waveformat,head)) #unpack waveform into container
	waveforms = doandshift(waveforms, offset)
	return headers,waveforms,wavelength




def definetrap(rise,top,tau,num): #this function defines the trapezoidal filter to be used
	filt=np.zeros(num)
	for i in range(rise):
		filt[i]=i+tau
		filt[i+rise+top]=rise-tau-i
	for i in range(rise,rise+top):
		filt[i]=rise
	for i in range(rise+rise+top,num):
		filt[i]=0
	scale=1.0/(rise*tau)
	filt*=scale
	return filt

def energyt0(waves,filt,t0shift,threshold): #this function returns
  energies=np.zeros(len(waves))
  t0s=np.zeros(len(waves))
  maxlocs=np.zeros(len(waves))
  lcrosses=np.zeros(len(waves))
  rcrosses=np.zeros(len(waves))
  for i in range(len(waves)):
    res=signal.convolve(waves[i],filt,mode='full')
    res=res[:len(filt)]
    #now find the max value and then the crossing points
    maxval=max(res)
    maxloc=np.argmax(res)
    maxlocs[i]=maxloc
    crossval=threshold*maxval
    j=maxloc-t0shift
    leftcross=0
    while(j<maxloc and j < len(filt)-1):
      if(res[j]<=crossval and res[j+1]>=crossval):
        leftcross=j+0.5
        j=maxloc
      j=j+1
    lcrosses[i]=leftcross
    rightcross=0
    j=maxloc
    while(j<maxloc+t0shift and j < len(filt)-1):
      if(res[j]>=crossval and res[j+1]<=crossval):
        rightcross=j+0.5
        j=maxloc+t0shift
      j=j+1
    rcrosses[i]=rightcross
    midpoint=int((rightcross+leftcross)/2)
    energies[i]=np.mean(res[midpoint-5:midpoint+5])
    t0s[i]=midpoint-t0shift
  return energies,t0s,maxlocs,lcrosses,rcrosses

def outputresults(filename, headers, energies, t0s):
	#first figure out the output file name
	outstart, extension = os.path.splitext(filename)
	outname = outstart +'_trapres.txt' #to signifiy its the results from the trap filter
	with open(outname, 'w') as f:
		f.write('EntryNumber Result EventID Board Channel Timestamp Length Energy T0\n')
		for i in range(len(headers)):
			f.write(str(int(i))+' '+str(bool(headers[i][0])) +' '+ str(int(headers[i][1])) +' '+  str(int(headers[i][2])) +' '+  str(int(headers[i][3])) +' '+  str(headers[i][4]) +' '+  str(int(headers[i][5])) +' '+  str(float(energies[i])) +' '+  str(float(t0s[i])) + '\n')



#now that all of the functions I'm going to use are defined, here is the code that actually runs
def doAnalysis(filename, offset, rise, top, tau, threshold):
	headers,waveforms,wavelength = load(filename, offset)
	filt = definetrap(rise, top, tau, wavelength) #i assume a waveform length of 3500 here
	t0shift=int(0.5*top + rise)
	energies, t0s, maxlocs, lcrosses, rcrosses = energyt0(waveforms,filt,t0shift,threshold)
	outputresults(filename, headers, energies, t0s)
	#now make the histogram

	#edit these parameters as you wish
	binwidth = 10
	startenergy = 0
	stopenergy = 1000
	bins=np.arange(startenergy,stopenergy,binwidth)
	plt.hist(energies,bins=bins)	
	#for logarithmic scaling on the y axis, uncomment this next line
	#plt.yscale('log')
	outstart, extension = os.path.splitext(filename)
	pdfname=outstart+str('_traphist.pdf')	
	plt.xlabel('Energy')
	plt.ylabel('Count')
	plt.title(outstart+' Energy Histogram')
	#determine output file name	
	plt.savefig(pdfname)

#requires load to be run before before plotwaveform.	
def plotWave(waveform, rise, top, tau, threshold):
	filt = definetrap(rise, top, tau, len(waveform)) #i assume a waveform length of 3500 here
	plt.plot(waveform)
	res=signal.convolve(waveform,filt,mode='full')
	res=res[:len(filt)]
	plt.plot(res)
	plt.show()

	
